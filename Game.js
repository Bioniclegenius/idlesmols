Game = {
    handlers: [],       //Handlers for various threads - add custom ones here
    mainTick: null,     //Tick thread
    ticksPerSecond: 10, //ticks per second
    saveRate: 30,       //Seconds between autosaves
    version: "0.0.1a",  //Version number
    addHandler: function(interval, func){
        this.handlers.push({
            func: func,
            interval: interval,
            phase: 0
        });
    },
    
    tabs: {
        Resources: {
            buttons: [
                {
                    text: "Collect wood",
                    id: "gainWood"
                },
                {
                    text: "Throw wood on the fire",
                    id: "fuelFireWood"
                },
                {
                    text: "Throw charcoal on the fire",
                    id: "fuelFireCharcoal"
                }
            ]
        },
        Crafts: {
        },
        Science: {
            buttons: [
                {
                    text: "Discover Fire",
                    id: "discoverFire"
                }
            ]
        },
        Smols: {
        },
        Options: {
            buttons: [
                {
                    text: "Save Game",
                    onClick: function(){
                        Game.Utils.Save();
                    }
                },
                {
                    text: "Load Game",
                    onClick: function(){
                        Game.Utils.Load();
                    }
                },
                {
                    text: "Wipe Save",
                    onClick: function(){
                        if(confirm("This will permanently erase all your save data. Are you sure you want to do this?"))
                            Game.Utils.Wipe();
                    }
                }
            ]
        }
    },
    
    Resources: {
        ClickButton: function(buttonID){
            switch(buttonID){
                case "gainWood":
                    Game.Resources.GainRes("wood", Game.Effects.effects["woodBase"]);
                    break;
                case "fuelFireWood":
                    Game.Resources.FuelFire("wood");
                    break;
                case "fuelFireCharcoal":
                    Game.Resources.FuelFire("charcoal");
                    break;
            }
        },
        
        FuelFire: function(res){
            if(Game.Resources.items[res].amt >= Game.Effects.GetEffect("fireFuelQuantity")){
                Game.Resources.GainRes(res, -1 * Game.Effects.GetEffect("fireFuelQuantity"));
                Game.Resources.GainRes("fire", Game.Effects.GetEffect("firePer" + Game.Utils.CapFirstChar(res)));
            }
        },
        
        GainRes: function(resource, amt){
            var modified = false;
            if(amt != 0){
                if(Game.Resources.items[resource] != undefined){
                    var res = Game.Resources.items[resource];
                    if((res.amt < res.cap || amt < 0) && (res.amt > 0 || amt > 0)){
                        res.amt += amt;
                        if(res.amt > res.cap && amt > 0)
                            res.amt = res.cap;
                        if(res.amt < 0 && amt < 0)
                            res.amt = 0;
                        modified = true;
                    }
                }
            }
            return modified;
        },
        
        UnlockResource: function(resource){
            if(Game.Resources.items[resource] != undefined)
                Game.Resources.items[resource].unlocked = true;
        },
        
        Save: function(saveData){
            saveData["Resources"] = JSON.stringify(Game.Resources.items);
        },
        
        Load: function(saveData){
            var resData = JSON.parse(saveData["Resources"]);
            for(var i in resData){
                Game.Resources.items[i].amt = resData[i].amt;
                Game.Resources.items[i].cap = resData[i].cap;
                Game.Resources.items[i].unlocked = resData[i].unlocked;
            }
        },
        
        CalcResPerTick: function(res){
            var income = Game.Effects.GetEffect(res + "PerTick");
            income *= Game.Effects.GetEffect(res + "Base") + 1;
            return income;
        },
        
        Update: function(){
            for(var i in Game.Resources.items){
                var res = Game.Resources.items[i];
                
                var gained = Game.Resources.GainRes(i, Game.Resources.CalcResPerTick(i));
                res.amt = Game.Utils.Round(res.amt);
                if(i == "fire" && gained){//Fire hack to gain charcoal
                    Game.Resources.GainRes("charcoal", -1 * Game.Effects.effects["charcoalPerFire"] * Game.Resources.CalcResPerTick(i));
                    Game.Resources.items["charcoal"].amt = Game.Utils.Round(Game.Resources.items["charcoal"].amt);
                }
                
                if(res.amt > 0)
                    res.unlocked = true;
                
                if(!res.unlocked)//Hide resources on left pane if they aren't unlocked
                    $("#" + res.displayName).hide();
                else
                    $("#" + res.displayName).show();
                
                $("#" + i + "Amt").text(Game.Utils.Beautify(res.amt));
                $("#" + i + "Cap").text(Game.Utils.Beautify(res.cap));
            }
            
            if(Game.Resources.items["fire"].unlocked)//Special case for fire button
                $("#ResourcesButtonfuelFireWood").show();
            else
                $("#ResourcesButtonfuelFireWood").hide();
            if(Game.Resources.items["charcoal"].unlocked)//Special case for fire button
                $("#ResourcesButtonfuelFireCharcoal").show();
            else
                $("#ResourcesButtonfuelFireCharcoal").hide();
        },
        
        items: {
            fire: {
                displayName: "Fire",
                color: "#FF4A00",
                amt: 0,
                cap: 30,
                unlocked: false
            },
            wood: {
                displayName: "Wood",
                amt: 0,
                cap: 100,
                unlocked: true
            },
            charcoal: {
                displayName: "Charcoal",
                amt: 0,
                cap: 100,
                unlocked: false
            }
        }
    },
    
    Science: {
        ClickButton: function(buttonID){
            Game.Science.Research(buttonID);
        },
        
        Research: function(tech){
            if(Game.Science.items[tech] != undefined){
                var sci = Game.Science.items[tech];
                if(sci != undefined && !sci.researched){
                    for(var i in sci.prices){//First check if we can afford it
                        if(Game.Resources.items[i] < sci.prices[i])
                            return false;//Quit out if we can't
                    }
                    for(var i in sci.prices)//Buy it
                        Game.Resources.GainRes(i, -1 * sci.prices[i]);
                    for(var i in sci.unlocks)//Unlock the stuff it controls
                        Game.Utils.Unlock(sci.unlocks[i]);
                    sci.researched = true;//Finish up and mark it researched
                }
            }
            else
                console.log("Couldn't find science tech for \"" + tech + "\"");
        },
        
        Update: function(){
            for(var i in Game.Science.items){
                var button = "#" + Game.Utils.FormatButtonID("Science", i);
                if($(button).length > 0){
                    if(Game.Utils.Unlocked(Game.Science.items[i].unlock))//Check if requirements fulfilled
                        Game.Science.items[i].unlocked = true;
                    if(!Game.Science.items[i].unlocked)//Hide it if it's not unlocked yet
                        $(button).hide();
                    else
                        $(button).show();
                    if($(button).className != "Completed"){
                        $(button).removeAttr("disabled");
                        for(var j in Game.Science.items[i].prices)
                            if(Game.Resources.items[j].amt < Game.Science.items[i].prices[j])
                                $(button).attr("disabled", "disabled");
                        if(Game.Science.items[i].researched){
                            $(button).attr("disabled", "disabled");
                            $(button).addClass("completed");
                        }
                    }
                }
            }
        },
        
        Save: function(saveData){
            saveData["Science"] = {};
            for(var i in Game.Science.items)
                saveData["Science"][i] = {
                    researched: Game.Science.items[i].researched,
                    unlocked: Game.Science.items[i].unlocked
                };
        },
        
        Load: function(saveData){
            for(var i in saveData["Science"]){
                Game.Science.items[i].researched = saveData["Science"][i].researched;
                Game.Science.items[i].unlocked = saveData["Science"][i].unlocked;
                if(saveData["Science"][i])
                    for(var j in Game.Science.items[i].unlocks)
                        Game.Utils.Unlock(Game.Science.items[i].unlocks[j]);
            }
        },
        
        items: {
            discoverFire: {
                prices: {
                    wood: 10
                },
                unlock: {
                    wood: 5
                },
                unlocked: false,
                researched: false,
                unlocks: [
                    "fire"
                ]
            }
        }
    },
    
    Effects: {
        GetEffect: function(effect){
            if(Game.Effects.effects[effect] == undefined)
                return 0;
            return Game.Effects.effects[effect];
        },
        
        effects: {
            woodBase: 1,
            firePerTick: -0.1,
            charcoalPerFire: 1,
            firePerWood: 1,
            firePerCharcoal: 5,
            fireFuelQuantity: 1
        }
    },
    
    Utils: {
        Beautify: function(i, numDigits = 3){
            return i;
        },
        
        Round: function(i, numDigits = 3){
            i *= Math.pow(10, numDigits);
            i = Math.round(i);
            i /= Math.pow(10, numDigits);
            return i;
        },
        
        Unlock: function(item){
            for(var i in Game.tabs)
                if(Game[i] != undefined)
                    if(Game[i].items != undefined)
                        for(var j in Game[i].items)
                            if(j == item)
                                Game[i].items[j].unlocked = true;
        },
        
        Unlocked: function(requirements){
            var unlocked = true;
            for(var r in requirements)
                for(var i in Game.tabs)
                    if(Game[i] != undefined)
                        if(Game[i].items != undefined)
                            for(var j in Game[i].items){
                                if(j == r){
                                    if(i == "Science"){
                                        if(!Game[i].items[j].researched)//Science techs have no amount, just whether or not they exist
                                            unlocked = false;
                                    }
                                    else
                                        if(Game[i].items[j].amt < requirements[r])//Everything else should go off amount
                                            unlocked = false;
                                }
                            }
            return unlocked;
        },
        
        Save: function(){
            var saveData = {
                SaveVersion: 1.0
            };
            
            Game.Resources.Save(saveData);
            Game.Science.Save(saveData);
            
            var saveDataString = JSON.stringify(saveData);
            saveDataString = btoa(saveDataString);      //Base64 it for storage
            localStorage.setItem("smolSave", saveDataString);
            
            console.log("Game saved.")
        },
        
        Load: function(){
            var saveDataString = localStorage.getItem("smolSave");
            if(saveDataString != null){
                saveDataString = atob(saveDataString);  //Un-Base64 it
                var saveData = JSON.parse(saveDataString);
                
                Game.Resources.Load(saveData);
                Game.Science.Load(saveData);
            }
            
            console.log("Game loaded.");
        },
        
        Wipe: function(){
            localStorage.removeItem("smolSave");
            location.reload();
        },
        
        ClickButton: function(button){
            var id = button.id;
            var components = id.split("Button");//Array of [Tab name, button id name]
            Game[components[0]].ClickButton(components[1]);
        },
        
        FormatButtonID: function(tab, buttonID, buttonNumber){
            return tab + "Button" + (buttonID != undefined? buttonID : buttonNumber);
        },
        
        CapFirstChar: function(string){
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    },
    
    Init: function(){        
        //=======================================================================================
        //Load save data
        //=======================================================================================
        
        try{
            Game.Utils.Load();
        }
        catch(err){
            console.log("Error caught in loading save data.");
            console.log(err);
        }
        
        //=======================================================================================
        //Tabs
        //=======================================================================================
        
        for(var i in Game.tabs){
            $(".tabrow").append("<li>" + i + "</li>\r\n");
            $(".bottom-right").append("<div class=\"tab\" id=\"" + i + "\"></div>\r\n");
            //=======================================================================================
            //Buttons
            //=======================================================================================
            for(var j in Game.tabs[i].buttons){
                var button = Game.tabs[i].buttons[j];
                $("#" + i).append("<button id=\"" + Game.Utils.FormatButtonID(i, button.id, j) + "\">" + button.text + "</button>\r\n");
                if(button.onClick != undefined)
                    $("#" + Game.Utils.FormatButtonID(i, button.id, j)).click(button.onClick);
                else
                    $("#" + Game.Utils.FormatButtonID(i, button.id, j)).click(function(){Game.Utils.ClickButton(this);});
            }
        }
        $(".tabrow").children(":first").addClass("selected")
        $(".tab").hide();
        $("#" + $(".selected:first").text()).show();
        
        $("li").click(function(e) {
            e.preventDefault();
            $("li").removeClass("selected");
            $(this).addClass("selected");
            $(".tab").hide();
            $("#" + $(".selected:first").text()).show();
        });
        
        //=======================================================================================
        //Resources on left pane
        //=======================================================================================
        
        for(var i in Game.Resources.items){
            var res = Game.Resources.items[i];
            var span = "<span id=\"" + res.displayName + "\">";
            if(res.color != undefined)
                span += "<span style=\"color: " + res.color + ";\">" + res.displayName + "</span>";
            else
                span += res.displayName;
            span += ": <span id=\"" + i + "Amt\"></span>/<span id=\"" + i + "Cap\"></span>\r\n<br />\r\n</span>\r\n";
            $(".bottom-left").append(span);
        }
        
        //=======================================================================================
        //Startup handlers
        //=======================================================================================
        
        Game.addHandler(1, Game.Update);
        Game.addHandler(Game.ticksPerSecond * Game.saveRate, Game.Utils.Save);
        
        //=======================================================================================
        //Start up main game tick
        //=======================================================================================
        
        if(typeof(Worker) !== "undefined"){//Web worker supported
            if(typeof(Game.worker) == "undefined"){
			var blob = new Blob([
				"onmessage = function(e) { setInterval(function(){ postMessage('tick'); }, 1000 / " + Game.ticksPerSecond + "); }"]);
                var blobURL = window.URL.createObjectURL(blob);
                Game.worker = new Worker(blobURL);
                Game.worker.addEventListener('message', Game.Tick);
                Game.worker.postMessage("tick");
            }
        }
        else//Web worker not supported
            Game.mainTick = setInterval(Game.Tick, 1000 / Game.ticksPerSecond);
            
        //=======================================================================================
        //Minor cleanup details
        //=======================================================================================
        
        $(".version").text(Game.version);
            
        //=======================================================================================
        //Show screen after loading
        //=======================================================================================
        
        $("#left").css("display", "flex");
        $("#right").css("display", "flex");
    },
    
    Tick: function(){
        for(var i in Game.handlers){
            Game.handlers[i].phase++;
            if(Game.handlers[i].phase >= Game.handlers[i].interval){
                Game.handlers[i].phase = 0;
                try{
                    Game.handlers[i].func();
                }
                catch(err){
                    console.log(err);
                }
            }
        }
    },
    
    Update: function(){
        Game.Resources.Update();
        Game.Science.Update();
        
        for(var i in Game.tabs){
            for(var j in Game.tabs[i].buttons){
                if($("#" + i + "Button" + j).is(":hidden"))
                    if(Game.tabs[i].buttons[j].unlock != undefined)
                        if(Game.tabs[i].buttons[j].unlock())
                            $("#" + i + "Button" + j).show();
            }
        }
    }
};

$(function(){
    Game.Init();
});